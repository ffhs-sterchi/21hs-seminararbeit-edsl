# Seminararbeit

[![Seminararbeit PDF](https://badgen.net/badge/color/als%20PDF/cyan?label=Seminararbeit)](https://gitlab.com/ffhs-sterchi/21hs-seminararbeit-edsl/-/raw/master/assets/Seminararbeit_Sterchi_Daniel_FS21_FFHS.pdf?inline=false)

### <sup>Eine domänenspezifische Sprache zur Vorbeugung von inkonsistenten Werten<br>bezüglich des Umsystems in *trigger-action* Konfigurationen</sup>


<small>
von <b>Daniel Sterchi</b><br>
im <b>Frühlingssemester 2021</b><br>
an der <b>Fernfachhochschule Schweiz (FFHS)</b><br>
</small>

## Sehenswertes

- [LightOffOnSunrise.kt](src/main/kotlin/ch/novarx/ffhs/automation/LightOffOnSunrise.kt) ist eine Automatisierung welche in der eDSL geschrieben ist
- [dsl.kt](src/main/kotlin/ch/novarx/ffhs/dsl/dsl.kt) beinhaltet die für die eDSL notwendigen Funktionen
- [entities.kt](src/main/kotlin/ch/novarx/ffhs/dsl/entities/entities.kt) ist das automatisch generierte File, welches die benötigten Informationen aus Home-Assistant beinhaltet
- [CompileErrorTest.kt](src/test/kotlin/ch/novarx/ffhs/automation/CompileErrorTest.kt) enthält die Unit-Tests, welche die Compile-Errors prüfen

## Applikaltion ausführen

```sh
# Home-Assistant mit Docker starten
docker-compose -f ./assets/home-assistant-docker/docker-compose.yml up -d
# Server für DSL starten
mvn compile quarkus:dev
```

### System-Vorbedingungen

- [Java Developement Kit 11](https://www.oracle.com/ch-de/java/technologies/javase-jdk11-downloads.html)
- [Maven](https://maven.apache.org/download.cgi) >= 3.6.3
- [Docker Compose](https://docs.docker.com/get-docker/)

Damit der DSL-Server eine Verbindung zu Home-Assistant (HA) aufbauen kann, muss noch im Root-Verzeichnis des Repository eine Datei mit folgendem Inhalt erstellt werden:

```dotenv
HA_BEARER=<long_lived_access_token>
```

Wobei `<long_lived_access_token>` mit einem effektiven _Long-lived access token_ der laufenden HA-Instanz befüllt werden muss. Ein solches Token kann im 
[Benutzerprofil](http://localhost:8123/profile) von HA generiert werden, Details sind in der 
[HA-Dokumentation](https://www.home-assistant.io/docs/authentication/#your-account-profile).

## Übersicht

Dieser PoC ist eine statisch-typisierte eDSL, welche auf das Heimautomatisierungssystem _Home-Assistant_ (abk. HA)
ausgelegt ist. Durch die eDSL können minimale _trigger-action_ Automatisierungen in der Sprache _Kotlin_ geschrieben
werden, welche dann HA-konforme YAML-Dateien generiert.

Die Applikation ist in drei Bereiche (_packages_) gegliedert (vgl. Abbildung unten).
Das Package [`dsl`](/src/main/kotlin/ch/novarx/ffhs/dsl) ist das Kernstück der eDSL, darin enthalten sind alle Klassen und Funktionen um eine Automatisierung schreiben zu können. Diese Automatisierungen sind allerdings im [`automation`](/src/main/kotlin/ch/novarx/ffhs/automation/LightOffOnSunrise.kt) Bereich angesiedelt, dass lediglich um eine klare Trennung zu schaffen, wo die Definitionen hingehören. Das [`generator`](/src/main/kotlin/ch/novarx/ffhs/generator)-Package ist schliesslich für die Abfrage von Daten aus HA zuständig und deren Umwandlung in einen Teil der eDSL.

```plantuml
@startuml
skinparam defaultTextAlignment center
skinparam shadowing false

skinparam agent {
    BorderColor #464646
    BackgroundColor white
}
skinparam cloud {
    BorderColor #464646
    BackgroundColor white
}
skinparam collections {
    BorderColor #464646
    BackgroundColor white
}
skinparam Arrow {
    Color #464646
    FontColor slategrey
}




cloud "Home-Assistant" as HA

package generator {
    agent GeneratorResource
    agent EntityService
    agent KotlinGeneratorService
    agent HomeAssistantAdapter
GeneratorResource -> EntityService
EntityService --> HomeAssistantAdapter
EntityService --> KotlinGeneratorService
EntityService --> EntityService : generate\nentities
}

package dsl {
    collections Entities
    agent DslFunctions
}

package automation #lightslategrey {
    collections "Automation xyz" as automations
}

automations --> Entities : reference
automations --> DslFunctions : use
KotlinGeneratorService --> Entities : generate

HA <-- HomeAssistantAdapter : fetch\nentities

@enduml

```
