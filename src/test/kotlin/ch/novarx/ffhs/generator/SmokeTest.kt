package ch.novarx.ffhs.generator

import ch.novarx.ffhs.generator.adapter.HomeAssistantAdapter
import ch.novarx.ffhs.generator.adapter.State
import io.quarkus.test.Mock
import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured
import org.assertj.core.api.Assertions
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.hamcrest.Matchers.containsString
import org.junit.jupiter.api.Test
import java.io.File
import javax.enterprise.context.ApplicationScoped

@QuarkusTest
class SmokeTest {
    @ConfigProperty(name = "ch.novarx.ffhs.entities_base_path")
    lateinit var BASE_PATH: String

    @Test
    fun should_get_lightsOffOnSunrise_automation() {
        RestAssured.given()
            .`when`().get("/generator/lightsOffOnSunrise")
            .then().body(
                containsString(
                    """
                    alias: "Lights out"
                    trigger:
                    - platform: "sun"
                      event: "sunrise"
                      offset: "00:30:00"
                    action:
                    - service: "switch.turn_off"
                      target:
                        entity_id: "switch.light_bedroom"
                    """.trimIndent()
                )
            )
            .statusCode(200)
    }

    @Test
    fun should_generate_entities() {
        RestAssured.given()
            .`when`().get("/generator/generate")
            .then()
            .statusCode(200)

        Assertions.assertThat(File("${BASE_PATH}/entities.kt").readText())
            .startsWith("package ch.novarx.ffhs.automation")
            .contains("val MySensor = Entity(\"my-sensor\", GenericPlatform)")
    }
}

@Mock
@RestClient
@ApplicationScoped
internal class MockHomeAssistantAdapter : HomeAssistantAdapter {
    override fun getStates(): Set<State?>? {
        return setOf(
            State("sensor.my-sensor", null, null, null, null, null)
        )
    }
}
