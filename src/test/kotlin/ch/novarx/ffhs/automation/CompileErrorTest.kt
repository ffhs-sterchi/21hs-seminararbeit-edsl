package ch.novarx.ffhs.automation

import org.assertj.core.api.AbstractThrowableAssert
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import javax.script.ScriptEngine
import javax.script.ScriptEngineManager
import javax.script.ScriptException


class CompileErrorTest {
    private lateinit var engine: ScriptEngine

    @BeforeEach
    fun initEngine() {
        engine = ScriptEngineManager().getEngineByExtension("kts")!!
    }

    @Test
    fun shouldBePossibleTo_turnOffLightPlatformEntity() {
        assertThat(LightBedroom.platform).isInstanceOf(LightPlatform::class.java)
        engine.eval("LightBedroom".inTurnOffAutomation())
    }

    @Test
    fun shouldNotBePossibleTo_turnOffGenericPlatformEntity() {
        assertThat(Sun.platform).isInstanceOf(GenericPlatform::class.java)
        assertCompileError("Sun".inTurnOffAutomation()).hasMessageContaining(
            "is Entity<GenericPlatform> but Entity<LightPlatform> was expected"
        )
    }

    @Test
    fun shouldNotBePossibleTo_turnOffNonExistingEntity() {
        assertCompileError("MyLight".inTurnOffAutomation()).hasMessageContaining(
            "Unresolved reference: MyLight"
        )
    }

    private fun assertCompileError(kotlinCode: String): AbstractThrowableAssert<*, *> {
        return assertThatThrownBy { engine.eval(kotlinCode) }.isInstanceOf(ScriptException::class.java)
    }

    private fun String.inTurnOffAutomation(): String = """
                package ch.novarx.ffhs.automation
                val lightsOffOnSunrise = automation("Lights out") {
                    triggers {
                        sun {
                            event = SunEvent.sunrise
                            offset = "00:30:00"
                        }
                    }
                    actions {
                        turn_off($this)
                    }
                }
            """
}
