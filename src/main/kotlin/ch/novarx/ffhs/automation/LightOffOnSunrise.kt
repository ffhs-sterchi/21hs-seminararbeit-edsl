package ch.novarx.ffhs.automation

val lightsOffOnSunrise = automation("Lights out") {
    triggers {
        sun {
            event = SunEvent.sunrise
            offset = "00:30:00"
        }
    }
    actions {
        turn_off(LightBedroom)
    }
}
