package ch.novarx.ffhs.generator.control

import ch.novarx.ffhs.generator.adapter.HomeAssistantAdapter
import io.quarkus.scheduler.Scheduled
import org.eclipse.microprofile.rest.client.inject.RestClient
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class EntityService {
    @Inject
    @RestClient
    lateinit var adapter: HomeAssistantAdapter

    @Inject
    lateinit var kotlinGeneratorService: KotlinGeneratorService

    fun generateEntitiesAsKotlin(): String {
        return kotlinGeneratorService.generateEntitiesAsKotlin(getEntityIds())
    }

    @Scheduled(every = "20s")
    fun generateEntitiesPeriodically(): Unit {
        generateEntitiesAsKotlin()
    }

    fun getEntityIds(): List<String> {
        return adapter.getStates()!!
            .filterNotNull()
            .mapNotNull { it.entityId }
    }
}
