package ch.novarx.ffhs.generator.control

import org.eclipse.microprofile.config.inject.ConfigProperty
import java.io.File
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class KotlinGeneratorService {
    private val LF = "\n"
    private val PACKAGE = "package ch.novarx.ffhs.automation$LF$LF"

    @ConfigProperty(name = "ch.novarx.ffhs.entities_base_path")
    lateinit var BASE_PATH: String

    fun generateEntitiesAsKotlin(entityIds: List<String>): String {
        val entitiesAsKotlin = entityIds.toKotlin()

        val file = File("${BASE_PATH}/entities.kt")
        file.createNewFile()
        file.writeText(entitiesAsKotlin)

        return entitiesAsKotlin
    }

    private fun aEntityValOf(entityId: String): String {
        val propertyName = propertyNameFor(entityId)
        val quotedId = idFor(entityId).quote()
        val platformClass = platformFor(entityId)
        return "val $propertyName = Entity($quotedId, $platformClass)"
    }


    private fun idFor(entityId: String): String {
        return entityId.split(".").drop(1).joinToString(".")
    }

    private fun propertyNameFor(entityId: String): String {
        return idFor(entityId).split(".", "_", "-", " ")
            .map { it.capitalize() }
            .joinToString("")
    }

    private fun platformFor(entityId: String): String {
        val possiblePlatformsToClassName = mapOf(
            Pair("light", "LightPlatform"),
            Pair("person", "PersonPlatform")
        )
        val entityPlatform = entityId.split(".")[0]
        return possiblePlatformsToClassName[entityPlatform] ?: "GenericPlatform"
    }

    fun List<String>.toKotlin(): String {
        return PACKAGE +
                this.map { aEntityValOf(it) }.sorted().joinToString(LF) +
                LF
    }

    fun String.quote(): String = "\"$this\""
}
