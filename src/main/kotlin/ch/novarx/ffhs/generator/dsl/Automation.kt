package ch.novarx.ffhs.generator.dsl

import ch.novarx.ffhs.automation.Actions
import ch.novarx.ffhs.automation.Triggers

class Automation(
    val alias: String,
    val trigger: Triggers? = null,
    val action: Actions? = null
)
