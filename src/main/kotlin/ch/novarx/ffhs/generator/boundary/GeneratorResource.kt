package ch.novarx.ffhs.generator.boundary

import ch.novarx.ffhs.automation.lightsOffOnSunrise
import ch.novarx.ffhs.generator.control.EntityService
import ch.novarx.ffhs.generator.dsl.Automation
import javax.inject.Inject
import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.ok

@Path("/generator")
@Consumes(MediaType.APPLICATION_JSON)
class GeneratorResource {

    @Inject
    lateinit var entityService: EntityService

    @GET
    @Path("lightsOffOnSunrise")
    @Produces("application/yaml")
    fun get(): Automation {
        return lightsOffOnSunrise.build()
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("generate")
    fun generate(): Response {
        return ok(entityService.generateEntitiesAsKotlin()).build()
    }
}
