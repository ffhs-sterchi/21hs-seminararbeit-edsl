package ch.novarx.ffhs.generator.adapter

import com.fasterxml.jackson.annotation.JsonAlias
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import javax.ws.rs.GET
import javax.ws.rs.Path


@Path("/api")
@RegisterRestClient(baseUri = "http://localhost:8123")
@RegisterClientHeaders(HomeAssistantAuthHeader::class)
interface HomeAssistantAdapter {
    @GET
    @Path("/states")
    fun getStates(): Set<State?>?
}

data class State(
    @JsonAlias("entity_id")
    var entityId: String?,
    @JsonAlias("state")
    var state: String?,
    @JsonAlias("attributes")
    var attributes: Attributes?,
    @JsonAlias("last_changed")
    var lastChanged: String?,
    @JsonAlias("last_updated")
    var lastUpdated: String?,
    @JsonAlias("context")
    var context: Context?
)

data class Attributes(
    @JsonAlias("supported_color_modes")
    var supportedColorModes: List<String>?,
    @JsonAlias("friendly_name")
    var friendlyName: String?,
    @JsonAlias("supported_features")
    var supportedFeatures: Int?
)

data class Context(
    @JsonAlias("id")
    var id: String?,
    @JsonAlias("parent_id")
    var parentId: String?,
    @JsonAlias("user_id")
    var userId: String?
)
