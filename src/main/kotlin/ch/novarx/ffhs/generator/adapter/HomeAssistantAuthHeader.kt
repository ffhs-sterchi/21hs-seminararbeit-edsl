package ch.novarx.ffhs.generator.adapter

import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.rest.client.ext.ClientHeadersFactory
import org.jboss.resteasy.specimpl.MultivaluedMapImpl
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.core.MultivaluedMap


@ApplicationScoped
class HomeAssistantAuthHeader : ClientHeadersFactory {

    @ConfigProperty(name = "ch.novarx.ffhs.ha-bearer")
    lateinit var BEARER: String

    override fun update(
        incomingHeaders: MultivaluedMap<String?, String?>?,
        clientOutgoingHeaders: MultivaluedMap<String?, String?>?
    ): MultivaluedMap<String, String>? {
        val result: MultivaluedMap<String, String> = MultivaluedMapImpl()
        result.add("Authorization", "Bearer $BEARER")
        return result
    }
}
