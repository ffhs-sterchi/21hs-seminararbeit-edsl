package ch.novarx.ffhs.automation

fun automation(alias: String, context: AutomationContext.() -> Unit): AutomationBuilder {
    return AutomationBuilder(alias, context)
}

infix fun Actions.turn_off(entity: Entity<LightPlatform>): TurnOffAction {
    val action = TurnOffAction()
    action.target = Target(entity)
    this.add(action)
    return action
}

fun Triggers.sun(init: SunTrigger.() -> Unit): Trigger {
    val trigger = SunTrigger()
    trigger.init()
    this.add(trigger)
    return trigger
}

open class AutomationContext {
    protected lateinit var definedTriggers: Triggers
    protected lateinit var definedActions: Actions

    fun triggers(init: Triggers.() -> Unit): Triggers {
        definedTriggers = Triggers()
        definedTriggers.init()
        return definedTriggers
    }

    fun actions(init: Actions.() -> Unit): Actions {
        definedActions = Actions()
        definedActions.init()
        return definedActions
    }
}
