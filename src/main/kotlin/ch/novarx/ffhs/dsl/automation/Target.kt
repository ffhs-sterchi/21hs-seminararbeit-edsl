package ch.novarx.ffhs.automation

class Target<T : Platform>(private val entity: Entity<T>) {
    val entity_id: String
        get() = entity.entity_id

    fun entity() = entity
}
