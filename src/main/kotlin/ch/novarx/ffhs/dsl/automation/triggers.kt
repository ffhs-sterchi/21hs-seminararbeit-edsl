package ch.novarx.ffhs.automation

interface Trigger
class Triggers : ArrayList<Trigger>()
class SunTrigger : Trigger {
    val platform = SunPlatform.name
    lateinit var event: SunEvent
    lateinit var offset: String
}
enum class SunEvent {
    sunrise, sunset;

    override fun toString(): String = name
}
