package ch.novarx.ffhs.automation

interface Action
class TurnOffAction : Action {
    val service: String = ".turn_off"
        get() = "${target.entity().platform.name}${field}"
    lateinit var target: Target<LightPlatform>
}

class Actions : ArrayList<Action>()

