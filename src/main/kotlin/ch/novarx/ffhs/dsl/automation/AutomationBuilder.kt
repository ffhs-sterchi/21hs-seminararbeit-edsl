package ch.novarx.ffhs.automation

import ch.novarx.ffhs.generator.dsl.Automation

class AutomationBuilder(
    private val alias: String,
    private val context: AutomationContext.() -> Unit
) : AutomationContext() {
    fun build(): Automation {
        this.context()
        return Automation(alias, this.definedTriggers, this.definedActions)
    }
}
