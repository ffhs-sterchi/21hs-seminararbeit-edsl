package ch.novarx.ffhs.automation

abstract class Platform(
    val name: String
)

object LightPlatform : Platform("switch")
object SunPlatform : Platform("sun")
object PersonPlatform : Platform("person")
object GenericPlatform : Platform("generic")
