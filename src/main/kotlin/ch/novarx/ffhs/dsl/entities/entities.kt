package ch.novarx.ffhs.automation

val Home = Entity("home", GenericPlatform)
val HttpLogin = Entity("http_login", GenericPlatform)
val LightBathroom = Entity("light_bathroom", LightPlatform)
val LightBedroom = Entity("light_bedroom", LightPlatform)
val LightLivingroom = Entity("light_livingroom", LightPlatform)
val Novarx = Entity("novarx", PersonPlatform)
val Sun = Entity("sun", GenericPlatform)
val Updater = Entity("updater", GenericPlatform)
