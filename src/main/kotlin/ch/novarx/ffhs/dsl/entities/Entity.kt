package ch.novarx.ffhs.automation

class Entity<T : Platform>(private val id: String, val platform: T) {
    val entity_id = "${platform.name}.${id}"
}
